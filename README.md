# CroBot

## Instructions
Send a UDP message to Crobot's public IP with port 7014. Listen back for a reply!

## Connection Details
* IP: `54.70.144.92`
* Port: `7014`