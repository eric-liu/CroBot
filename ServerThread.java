import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.*;

public class ServerThread extends Thread {

    private static final int PORT = 7014;
    private static final int BUFFER_SIZE = 512;
    private static final long TO_NS = (long) 10e8;
    private static final long TO_MB = (long) 10e6;
    private static final long UPDATE_RATE = 5 * TO_NS;
    private static final long OLD_MESSAGE_THRESHOLD = 20 * TO_NS;
    private static final long BANDWIDTH_CAPACITY = 100 * TO_MB; // 100 MegaBytes

    private Map<String, User> users;
    private List<String> messages;
    private DatagramChannel channel;
    private long currentTime;

    public ServerThread() {
        users = new HashMap<>();
        messages = new ArrayList<>();
        addDefaultMessages(messages);

        try {
            channel = DatagramChannel.open();
            channel.configureBlocking(false);
            channel.socket().bind(new InetSocketAddress(PORT));
        } catch (IOException e) {
            e.printStackTrace();
        }

        currentTime = System.nanoTime();
    }

    @Override
    public void run() {

        SocketAddress address;
        long delta = 0;
        long totalBytesSent = 0;
        long recentBytesSent = 0;
        long messagesSent = 0;

        while (isAlive()) {

            if (totalBytesSent >= BANDWIDTH_CAPACITY) {
                System.err.printf("Bandwidth capacity reached - Messages: %d | Bytes: %d | Lifetime: %s\n",
                        messagesSent, totalBytesSent, new Date(currentTime).toString());
                break;
            }

            long startTime = System.nanoTime();

            ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
            buffer.clear();

            try {

                // Listen for new messages
                if ((address = channel.receive(buffer)) != null) {

                    // Modify port to 7014
                    address = new InetSocketAddress(((InetSocketAddress) address).getAddress(), 7014);

                    // Update message if existing user
                    if (users.containsKey(address.toString())) {
                        users.get(address.toString()).timeSinceLastMessage = currentTime;
                    } else { // Add new if non-existing user
                        users.put(address.toString(), new User(address));
                    }

                    // Extract data
                    String data = new String(buffer.array()).trim();
                    String[] split = data.split(":");
                    if (split.length == 2) {
                        data = split[1];
                    } else if (split.length == 1) {
                        data = split[0];
                    }

                    // Add if unique message
                    if (!messages.contains(data)) {
                        messages.add(data);
                    }

//                    System.out.printf("Received: %s (%s)\n",
//                            data,
//                            address); // TODO: DEBUG purposes

                    messagesSent++;
                } else {
                    if (delta >= UPDATE_RATE) {
                        System.out.printf("Bytes sent: %d (%d total bytes)\n", recentBytesSent, totalBytesSent);
                        recentBytesSent = 0;
//                        System.out.println("Sending messages");
                        // Remove old users
                        removeOldUsers();

                        // Distribute random message
                        String message = "crobot:" + getRandomMessage();
                        for (User user : users.values()) {
                            ByteBuffer buffer1 = ByteBuffer.allocate(BUFFER_SIZE);
                            buffer1.clear();
                            buffer1.put(message.getBytes());
                            buffer1.flip();
                            recentBytesSent += channel.send(buffer1, user.address);
//                            System.out.printf("Sending: %s (%s)\n",
//                                    new String(buffer1.array()).trim(),
//                                    user.address);
                        }

                        totalBytesSent += recentBytesSent;
                        delta -= UPDATE_RATE;
                    }
                }

                // Cap update rate to 10 times per second
                sleep(100);

            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }

            long endTime = System.nanoTime();
            delta += endTime - startTime;
            currentTime += endTime - startTime;

        }

    }

    private void removeOldUsers() {
        List<String> oldUsers = new ArrayList<>();

        // Find old users
        for (User user : users.values()) {
//            System.out.println(currentTime - user.timeSinceLastMessage);
            if (currentTime - user.timeSinceLastMessage >= OLD_MESSAGE_THRESHOLD) {
                oldUsers.add(user.address.toString());
            }
        }

        // Log and drop old users
        for (String user : oldUsers) {
            System.out.println("Dropping: " + user);
            users.remove(user);
        }
    }

    private void addDefaultMessages(List<String> list) {
        list.add("Crobot dreams to become a Crobat.");
        list.add("Crobot says: \"Keep your Jolteons away from water.\"");
        list.add("Crobot says: \"BlahBlahBlahPokemonBlahBlah.\"");
        list.add("Crobot wishes it were Julian Mestre ... for some reason.");
        list.add("Crobot smash! Crobot crash!");
        list.add("Crobot wishes you an apple. That's right. An apple.");
        list.add("Crobot doesn't know what message to send.");
        list.add("Crobot would like a hug.");
        list.add("Crobot requests that you send less inappropriate messages.");
        list.add("Crobot thanks you.");
        list.add("Crobot says: \"Crobot! Cro-cro!.\"");
    }

    private String getRandomMessage() {
        return messages.get((int) (Math.random() * messages.size()));
    }

}
