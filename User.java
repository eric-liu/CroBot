import java.net.SocketAddress;

public class User {

    public SocketAddress address;
    public long timeSinceLastMessage;

    public User(SocketAddress address) {
        this.address = address;
        this.timeSinceLastMessage = System.nanoTime();
    }


}
